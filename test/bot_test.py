import pytest
from messages import messages as msgs
from bot import send_welcome, registration_medical_policy_state_handle, cancel_book_employee, echo_handler

from unittest.mock import AsyncMock


@pytest.mark.asyncio
async def test_echo_handler():
    message_mock = AsyncMock(text="SomeText")
    await echo_handler(message=message_mock)
    assert message_mock.answer.call_count == 2
    assert message_mock.answer.call_args_list[0].args == ('ECHO',)
    assert message_mock.answer.call_args_list[1].args == ('SomeText',)